package com.pajato.argus.filter.core

import com.pajato.i18n.strings.StringsResource

public object I18nStrings {
    public const val FILTER_NOT_A_FILE: String = "FilterNotAFile"
    public const val FILTER_URI_ERROR: String = "FilterUriError"

    public fun registerStrings() {
        StringsResource.put(FILTER_NOT_A_FILE, "The given file with name '{{name}}' is not a valid file!")
        StringsResource.put(FILTER_URI_ERROR, "No URI has been injected! A valid file URI is required.")
    }
}
