package com.pajato.argus.filter.core

public class FilterRepoError(message: String) : Exception(message)
