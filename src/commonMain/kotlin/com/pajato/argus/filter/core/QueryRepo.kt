package com.pajato.argus.filter.core

import java.net.URI

public interface QueryRepo {
    public suspend fun injectDependency(uri: URI)
    public fun get(): List<String>
    public fun register(item: String)
    public fun remove(item: String)
}
