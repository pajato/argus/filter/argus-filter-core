package com.pajato.argus.filter.core

import kotlin.test.Test
import kotlin.test.assertEquals

class FilterRepoErrorUnitTest {
    @Test fun `When creating a network repo error instance, verify behavior`() {
        val message = "some message"
        val networkRepoError = FilterRepoError(message)
        assertEquals(message, networkRepoError.message)
    }
}
