package com.pajato.argus.filter.core

import com.pajato.argus.filter.core.I18nStrings.FILTER_NOT_A_FILE
import com.pajato.argus.filter.core.I18nStrings.FILTER_URI_ERROR
import com.pajato.i18n.strings.Arg
import com.pajato.i18n.strings.StringsResource
import com.pajato.i18n.strings.StringsResource.get
import com.pajato.test.ReportingTestProfiler
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class I18nStringsUnitTest : ReportingTestProfiler() {

    @BeforeTest fun setUp() {
        StringsResource.cache.clear()
        I18nStrings.registerStrings()
    }

    @Test fun `When accessing the localize network strings, verify behavior`() {
        val noUriMessage = "No URI has been injected! A valid file URI is required."
        val invalidPathMessage = "The given file with name 'z.txt' is not a valid file!"
        assertEquals(noUriMessage, get(FILTER_URI_ERROR))
        assertEquals(invalidPathMessage, get(FILTER_NOT_A_FILE, Arg("name", "z.txt")))
    }
}
