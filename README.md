# argus-filter-core

## Description

This project defines the
[Clean Code Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
"Enterprise Business Rules" layer (aka "Core") for the Argus filter feature. It is responsible for defining the
interfaces, classes and top level artifacts supporting video filtering using networks, genres, profiles and query
strings.

## License

GPL, Version 3.0.  See the peer document LICENSE for details.

## Contributions

See the [contributing guide](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/CONTRIBUTING.md) in the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

## Project status

Converted to Kotlin Multiplatform (KMP) with versions 0.10.*

## Documentation

For general documentation on Argus, see the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

As documentation entered into code files grows stale seconds after it is written, no such documentation is created.
Instead, documentation is created by you on demand using the Dokka Gradle task: 'dokkaGfm'. After successful task
completion, see the detailed documentation [here](build/dokka/gfm/index.md)

## Usage

To use the project, follow these steps:

1. Add the project as a dependency in your build file.
2. Import the necessary classes and interfaces from the project.
3. Use the provided APIs to interact with the shelf feature.

## Test Cases

The table below identifies the adapter layer unit tests. A test file name is always of the form `NamePrefixUnitTest.kt`.
The test file content is one or more test cases (functions)

| Filename Prefix  | Test Case Name                                               |
|------------------|--------------------------------------------------------------|
| I18nStrings      | When accessing the localized filter strings, verify behavior |
| FilterRepoError  | When creating a filter repo error instance, verify behavior  |

### Overview

### Notes

The single responsibility for this project is to provide the interface definitions used by outer architectural layers for
the Filter feature.

The interfaces adapter layer implements these core interfaces.

Specifically, the core Filter interfaces are Filter and FilterRepo.
