plugins {
    alias(libs.plugins.kmp.lib)
}

group = "com.pajato.argus"
version = "0.10.3"
description = "The Argus filter feature, core layer, KMP common target project"

kotlin.sourceSets {
    val commonMain by getting {
        dependencies {
            implementation(libs.kotlinx.serialization.json)
            implementation(libs.pajato.persister)
            implementation(libs.pajato.i18n.strings)
        }
    }

    val commonTest by getting {
        dependencies {
            implementation(libs.kotlin.test)
            implementation(libs.pajato.test)
        }
    }
}
